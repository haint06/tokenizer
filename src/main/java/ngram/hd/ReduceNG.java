package ngram.hd;

/**
 * Created by pc on 07/10/2016.
 */

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.pig.backend.executionengine.ExecException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class ReduceNG extends Reducer<Text, Text, Text, Text> {

    protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
        String token = key.toString();
        if (token == "0" || token == "-1")
            return;
        double sum=0;
        for (Text t: values){
            sum+= Double.valueOf(t.toString());
        }
        context.write(new Text(token), new Text(sum+""));
    }
}
