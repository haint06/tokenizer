package ngram.hd;

/**
 * Created by pc on 07/10/2016.
 */

import com.vcc.bigdata.logprs.parquet.schema.AdsGroup;
import com.vcc.bigdata.logprs.parquet.schema.PageViewV1Log;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;


public class MapNG extends Mapper<LongWritable, Text, Text, Text> {
    private Text value_word = new Text("");
    private Text key_word = new Text("");

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] var = line.split("\t");
        context.write(new Text(var[0]), new Text(var[1]));
    }
}