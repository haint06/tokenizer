package ngram;

import stemmer.StopWords;

import java.io.*;
import java.util.*;

/**
 * Created by pc on 07/10/2016.
 */
public class ReadFile {
    public static void main(String args[]) {
        FileInputStream in = null;
        FileWriter out = null;
        BufferedReader bf = null;
        FileInputStream in1 = null;
        FileWriter out1 = null;
        BufferedReader bf1 = null;

        try {
            Map<String, Double> token = new HashMap<>();
            Map<String, Double> map = new HashMap<>();
            List<NString> list = new ArrayList<>();
            for (int i = 0; i < 10; i++) {
                NString max = new NString();
                in = new FileInputStream("/home/pc/Desktop/ngram/part-r-0000" + i);
                bf = new BufferedReader(new InputStreamReader(in));
                String line;
                while ((line = bf.readLine()) != null) {
                    String[] splits = line.split("\t");
                    String[] ab = splits[0].split("\\+");
                    if (StopWords.getInstance().isStopword(ab[0]) || StopWords.getInstance().isStopword(ab[1])) {
                        continue;
                    }
                    String s = ab[1] + "+" + ab[0];
                    if (map.containsKey(s)) continue;
                    map.put(splits[0], Double.valueOf(splits[1]));
                }
            }
            System.out.println("Done 1");
            for (int i = 0; i < 10; i++) {
                NString max = new NString();
                in1 = new FileInputStream("/home/pc/Desktop/token/part-r-0000" + i);
                bf1 = new BufferedReader(new InputStreamReader(in1));
                String line;
                while ((line = bf1.readLine()) != null) {
                    String[] splits = line.split("\t");
                    if (StopWords.getInstance().isStopword(splits[0])) {
                        continue;
                    }
                    token.put(splits[0], Double.valueOf(splits[1]));
                }
            }
            System.out.println("Done 2");
            normalize(map);
            normalize(token);

            for (String s : map.keySet()) {
                String[] ab = s.split("\\+");
                String a = ab[0];
                String b = ab[1];
                double value = map.get(s) * Math.log(token.get(s) / (token.get(a) * token.get(b)));
                list.add(new NString(s, value));
            }
            Collections.sort(list);
            Collections.reverse(list);
            System.out.println("Step1: done");
            out = new FileWriter("UnNgrams.txt");
            for (NString nString : list) {
                out.append(nString.toString() + "\n");
            }
            out.flush();
            out.close();
        } catch (IOException e) {

        } finally {
        }
    }

    private static void normalize(Map<String, Double> map) {
        double sum = 0.0;
        for (String s : map.keySet()) {
            sum += map.get(s);
        }
        for (String s : map.keySet()) {
            map.put(s, map.get(s) / sum);
        }
    }
}
