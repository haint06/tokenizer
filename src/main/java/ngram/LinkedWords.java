package ngram;

import stemmer.StopWords;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc on 18/10/2016.
 */
public class LinkedWords {

    public static void main(String args[]){
        FileInputStream in = null;
        FileWriter out = null;
        BufferedReader bf = null;
        Map<String, Double> map= new HashMap<>();
        try{

            in = new FileInputStream("/home/pc/TFIDF/UnNgrams10000.txt");
            bf = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = bf.readLine()) != null) {
                String[] splits = line.split(":");
                if(splits[0]==null||splits[1]==null) continue;
//                System.out.println(line);
                if(splits.length>=3) continue;
                map.put(splits[0], Double.valueOf(splits[1]));
            }
            System.out.println("s 1");
            Map<String, Map<String, Double>> m= convert(map);
            System.out.println(m.get("iphone"));
        }catch (IOException e){

        }
        System.exit(1);
    }

    private static Map<String, Map<String, Double>> convert(Map<String, Double> m) {
        Map<String, Map<String, Double>> map = new HashMap<>();
        for (String s : m.keySet()) {
            System.out.println(s);
            String[] ab = s.split("\\+");
            Map<String, Double> temp = new HashMap<>();

            if (!map.containsKey(ab[0])){
                temp.put(ab[1], m.get(s));
                map.put(ab[0], temp);
            }else{
                temp= map.get(ab[0]);
                temp.put(ab[1], m.get(s));
                map.put(ab[0], temp);
            }

            if (!map.containsKey(ab[1])){
                temp.put(ab[0], m.get(s));
                map.put(ab[1], temp);
            }else{
                temp= map.get(ab[1]);
                temp.put(ab[0], m.get(s));
                map.put(ab[1], temp);
            }
        }
        return map;
    }
}
