package preprocess;

import app.VCTokenizer;
import config.Document;
import connectDB.Cassandra;
import connectDB.ConnectMySQL;
import stemmer.StopWords;

import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pc on 21/10/2016.
 */
public class LDAPre {

    public static void main(String args[]) throws SQLException, ClassNotFoundException {
        List<String> newsIds= ConnectMySQL.getInstance().getAllNews();
        System.out.println(newsIds.size());
//        List<Document> documents= new ArrayList<>();
        FileWriter fileWriter= null;
        try {
            fileWriter = new FileWriter(new File("lda-file/input-lda.txt"));
            for(String id: newsIds){
                System.out.println(id);
                String sentences= ConnectMySQL.getInstance().getContentFromNewsIDByMYSQL(id);
                String tags= ConnectMySQL.getInstance().getTags(id);
//                if(sentences.equals("")){
//                    sentences= ConnectMySQL.getInstance().getContentFromNewsIDByMYSQL(id);
//                }
                String content= getContent(sentences, tags);
                fileWriter.append(id+"\t"+content+"\n");
                fileWriter.flush();
            }
            fileWriter.close();
        }catch (IOException e){
            e.printStackTrace();
        }

//        tranferFile("lda-file/input-lda.txt");
        System.exit(1);
    }

    private static void tranferFile(String filePath){
        File file= new File(filePath);
        try{
            FileInputStream inputStream= new FileInputStream(file);
            BufferedReader bf= new BufferedReader(new InputStreamReader(inputStream));
            String line="";
            FileWriter fileWriter= new FileWriter("/home/pc/VCC/tokens.txt");
            while((line= bf.readLine())!=null){
                if(line.contains("\t")){
                    String tokens= line.split("\t")[1];
                    fileWriter.append(tokens+"\n");
                }
            }
            fileWriter.flush();
            fileWriter.close();
        }catch (Exception e){

        }
    }

    private static String getContent(String sentences, String tags) {
        String content = "";
        char c=92;
        sentences= normalize(sentences);
        sentences = sentences.replace(c, ' ').replaceAll("\\<.*?>", " ").replaceAll("\\[.*?]", " ");
        if (tags != null) {
            boolean flag = true;
            if (sentences != null) {
                sentences+=" . "+tags+" . "+tags;
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length; var5++) {
                        String var2 = normalize(var1[var5].replaceAll("987", "_"));
                        if(!StopWords.getInstance().isStopword(var2)){
                            content+=var2+" ";
                        }
                    }
                } catch (Exception e) {

                }
            }
        }else{
            if (sentences != null) {
                try {
                    String[] var1 = VCTokenizer.getInstance().getSegmenter().segment(sentences).split(" ");
                    for (int var5 = 0; var5 < var1.length; var5++) {
                        String var2 = normalize(var1[var5]);
                        if(!StopWords.getInstance().isStopword(var2)){
                            content+=var2+" ";
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
        return content;
    }

    private static String normalize(String var1) {
        String var2 = var1.toLowerCase();
        return var2;
    }
}
