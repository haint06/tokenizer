package test;


import app.NewToken;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import config.User;
import connectDB.Cassandra;
import stemmer.StopWords;

import java.io.*;
import java.util.*;

/**
 * Created by pc on 06/09/2016.
 */
public class QueryToFile {
    public static void main(String args[]) throws IOException {
        Cassandra.getInstance();
        updateKeyWord("2885620731906312862", "kenh14.vn");
//        System.out.println(Cassandra.getInstance().getMap("20161005143949829"));
        System.exit(1);
    }
    public static Map<String, Double> updateKeyWord(String guid, String domain) {
        Map<String, Double> longTermWords = new HashMap<>();
        Date date= new Date();
        int M = 5;
        double[] weights = {1, 0.9, 0.8, 0.7, 0.6, 0.5};
        Map<String, Double>[] temp = new Map[6];
        //        User []user= new User[N];
        for (int i = 0; i < 6; i++) {
            System.out.print(i+": ");
            User user = NewToken.getInstance().setUser(guid, domain, M * i, M * (i + 1));
            System.out.println(user.getMapTFIDF2());
            if (user.getMapTFIDF2().isEmpty()) {
                temp[i] = new HashMap<>();
            } else {
                temp[i] = NewToken.getInstance().getTopN(user.getMapTFIDF2(), 100);
            }
        }
        Set<String> words = new HashSet<>();
        for (int i = 0; i < 6; i++) words.addAll(temp[i].keySet());
        for (String s : words) if (StopWords.getInstance().isStopword(s)) words.remove(s);
        for (String s : words) {
            double sum = 0, sum2 = 0;
            for (int i = 0; i < 6; i++) {
                if (temp[i].containsKey(s)) {
                    sum += temp[i].get(s) * weights[i];
                    sum2 += 1 * weights[i];
                }
            }
            longTermWords.put(s, sum / 6 * sum2);
            longTermWords= NewToken.getTopN(longTermWords, 100);
        }
        System.err.println(longTermWords);
//        com.datastax.driver.core.Statement exampleQuery = QueryBuilder.insertInto("othernews", "long_term").value("guid_domain", guid + "_" + domain)
//                .value("keywords", longTermWords).value("time", date);
//        Cassandra.getInstance().getSession().execute(exampleQuery);
        return longTermWords;
    }
}
